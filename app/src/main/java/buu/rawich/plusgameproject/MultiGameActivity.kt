package buu.rawich.plusgameproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import buu.rawich.plusgameproject.MainActivity.Companion.correctScore
import buu.rawich.plusgameproject.MainActivity.Companion.wrongScore
import kotlin.random.Random

class MultiGameActivity : AppCompatActivity() {

    companion object {
        var answerForQuestion = 0
        var btnArray = arrayListOf<Button>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_multi_game)
        btnArray = arrayListOf<Button>(
            findViewById(R.id.btnAns),
            findViewById(R.id.btnAns2),
            findViewById(R.id.btnAns3)
        )
        this.play()
        this.setBtnOnclick()
    }

    private fun setBtnOnclick () {
        for (btn in btnArray) btn.setOnClickListener { this.getResult(btn.text.toString().toInt()) }

        val btnReset = findViewById<Button>(R.id.btnReset)
        btnReset.setOnClickListener { this.resetScore() }

        val btnBack = findViewById<Button>(R.id.btnBack)
        btnBack.setOnClickListener {
            val intent = Intent(MultiGameActivity@this, MainActivity::class.java)
            startActivity(intent)
        }
    }

    private fun play () {
        val answer = this.setQuestion()
        val answerIndex = Random.nextInt(0,2)
        val answerArray = arrayListOf<Int>()
        answerForQuestion = answer
        answerArray.add(answer)
        this.setScore()

        for ((index, btn) in btnArray.withIndex()) {
            if (answerIndex == index) {
                btn.text = answer.toString()
            } else {
                val anotherAnswer = this.getAnotherAnswer(answer, answerArray)
                answerArray.add(anotherAnswer)
                btn.text = anotherAnswer.toString()
            }
        }
    }

    private fun getAnotherAnswer (realAnswer: Int, answerArray: ArrayList<Int>) : Int {
        while (true) {
            val anotherAnswer = Random.nextInt(0,realAnswer + 5)
            if (!answerArray.contains(anotherAnswer)) return anotherAnswer
            else continue
        }
    }

    private fun getResult (answer: Int) {
        val intent = Intent(PlayActivity@this, MainActivity::class.java)
        if (answer == answerForQuestion) {
            Toast.makeText(this@MultiGameActivity, "ถูกต้อง",  Toast.LENGTH_LONG).show()
            correctScore++
        } else {
            Toast.makeText(this@MultiGameActivity, "ไม่ถูกต้อง",  Toast.LENGTH_LONG).show()
            wrongScore++
        }
        startActivity(intent)
    }

    private fun setQuestion (): Int {
        val numFirst = Random.nextInt(0, 10)
        val numSecond = Random.nextInt(0, 10)
        this.setQuestionDisplay(numFirst, numSecond)
        return numFirst * numSecond
    }

    private fun setQuestionDisplay (numFirst: Int, numSecond: Int) {
        val txtNumFirst = findViewById<TextView>(R.id.txtRandom1)
        val txtNumSecond = findViewById<TextView>(R.id.txtRandom2)
        txtNumFirst.text = numFirst.toString()
        txtNumSecond.text = numSecond.toString()
    }

    private fun setScore () {
        val txtCorrectScore = findViewById<TextView>(R.id.txtCorrectScore)
        val txtWrongScore = findViewById<TextView>(R.id.txtWrongScore)
        txtCorrectScore.text = correctScore.toString()
        txtWrongScore.text = wrongScore.toString()
    }

    private fun resetScore () {
        correctScore = 0
        wrongScore = 0
        this.setScore()
    }

}
