package buu.rawich.plusgameproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_push_game.*

class MainActivity : AppCompatActivity() {

    companion object {
        var correctScore = 0
        var wrongScore = 0
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnPushGame = findViewById<Button>(R.id.btnPushGame)
        btnPushGame.setOnClickListener {
            val intent = Intent(MainActivity@this, PushGameActivity::class.java)
            startActivity(intent)
        }
        val btnMinGame = findViewById<Button>(R.id.btnMinGame)
        btnMinGame.setOnClickListener {
            val intent = Intent(MainActivity@this, MinGameActivity::class.java)
            startActivity(intent)
        }
        val btnMultiGame = findViewById<Button>(R.id.btnMultiGame)
        btnMultiGame.setOnClickListener {
            val intent = Intent(MainActivity@this, MultiGameActivity::class.java)
            startActivity(intent)
        }
        this.setScore()

    }
    private fun setScore () {
        val txtScoreCorrect = findViewById<TextView>(R.id.txtCorrectScore)
        val txtScoreInCorrect = findViewById<TextView>(R.id.txtWrongScore)
        txtScoreCorrect.text = correctScore.toString()
        txtScoreInCorrect.text = wrongScore.toString()
    }
}


